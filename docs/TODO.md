# Todo

- [x] Create 2FA API

  - [x] Setup Database
  - [x] Setup basic server
  - [x] IssueCheck
  - [x] Register
  - [x] Login
  - [x] OTP generation
  - [x] OTP verification
  - [x] OTP validation
  - [x] Disable OTP

---

- [x] Write a documentation
  - [x] For API
    - [x] Usage of routes
